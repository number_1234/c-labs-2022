#include <stdio.h>
// task 1, calc-f

int main(void) {
    float a, b;

    scanf("%f %f", &a, &b);
    printf("a + b = %g\n", a + b);
    printf("a - b = %g\n", a - b);
    printf("a * b = %g\n", a * b);
    printf("a / b = %g\n", a / b);

    return 0;
}
