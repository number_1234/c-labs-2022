#include <stdbool.h>
#include <string.h>
#include <stdio.h>
// task 2, set

struct Number {
    unsigned long long int decimal; // десятичное значение
    unsigned binArr[128]; // двоичная форма числа
    unsigned set[128]; // само множество
    int baCnt; // индекс, откуда начинаем в двоичной форме, чтобы не было незначащих нулей
    unsigned sCnt; // аналогичный индекс для десятичного множества
};


struct Result {
    unsigned setsUnion[128]; // объединение мн-в
    unsigned setsIntersection[128]; // пересечение мн-в
    unsigned setsDifference[128]; // разность мн-в
    unsigned setsSymmetricDifference[128]; // симметрическая разность
};


void getBinary(struct Number *num) { // ok
    int i;
    unsigned long long int decimalCopy = num->decimal;

    i = 0;
    while (decimalCopy > 0) {
        num->binArr[i] = decimalCopy % 2;
        decimalCopy >>= 1;
        i++;
    }

    if (i > 1) {num->baCnt = --i;}
    else {num->baCnt = 1;}
}


void printBinary(struct Number *num) { // ok
    int i = num->baCnt;
    for (; i >= 0; i--) {printf("%d", num->binArr[i]);}
    printf("\n");
}


void getDecimalSet(struct Number *num) { // ok
    int i;

    for (i = 0; i < 128; i++) {num->set[i] = -1;}

    num->sCnt = 0;
    for (i = num->baCnt; i >= 0; i--) {
        if (num->binArr[i] == 1) {
            num->set[num->sCnt] = i;
            num->sCnt++;
        }
    }
}


void printDecimalSet(unsigned set[128]) { // ok
    int i = 0;

    printf("{ ");
    while (set[i] != -1) {
        printf("%d ", set[i]);
        i++;
    }
    printf("}\n");
}


bool _valueInArray(unsigned arr[128], unsigned value) {
    for (int i = 0; i < 128; i++) {
        if (arr[i] == value) {return true;}
    }
    return false;
}


void getSetsIntersection(unsigned setA[128], unsigned setB[128], struct Result *r) { // ok
    int i, j = 0;

    for (i = 0; i < 128; i++) {r->setsIntersection[i] = -1;}
    i = 0;
    while ((setA[i] != -1) || (setB[i] != -1)) {
        if (_valueInArray(setB, setA[i]) && !_valueInArray(r->setsIntersection, setA[i])) {
            r->setsIntersection[j++] = setA[i];
        }

        if (_valueInArray(setA, setB[i]) && !_valueInArray(r->setsIntersection, setB[i])) {
            r->setsIntersection[j++] = setB[i];
        }

        i++;
    }
}


void getSetsUnion(unsigned setA[128], unsigned setB[128], struct Result *r) { // ok
    int i, j = 0;

    for (i = 0; i < 128; i++) {r->setsUnion[i] = -1;}
    
    i = 0;
    while ((setA[i] != -1) || (setB[i] != -1)) {
        if (!_valueInArray(r->setsUnion, setA[i])) {r->setsUnion[j++] = setA[i];}
        if (!_valueInArray(r->setsUnion, setB[i])) {r->setsUnion[j++] = setB[i];}
        i++;
    }
}


void getSetsDifference(unsigned setA[128], unsigned setB[128], struct Result *r) { // ok
    int i, j = 0;

    for (i = 0; i < 128; i++) {r->setsDifference[i] = -1;}

    i = 0;
    while (setA[i] != -1) {
        if (!_valueInArray(setB, setA[i])) {r->setsDifference[j++] = setA[i];}
        i++;
    }
}


void getSetsSymmetricDifference(unsigned setA[128], unsigned setB[128], struct Result *r) { // ok
    int i, j = 0;

    for (i = 0; i < 128; i++) {r->setsSymmetricDifference[i] = -1;}

    i = 0;
    while ((setA[i] != -1) || (setB[i] != -1)) {
        if (!_valueInArray(setB, setA[i])) {r->setsSymmetricDifference[j++] = setA[i];}
        if (!_valueInArray(setA, setB[i])) {r->setsSymmetricDifference[j++] = setB[i];}
        i++;        
    }
}


int main(void) {
    struct Number a, b;
    struct Result result;

    printf("Enter A and B (unsigned long integers): ");
    fflush(stdin);
    scanf("%llu %llu", &a.decimal, &b.decimal);

    getBinary(&a);
    getBinary(&b);
    printBinary(&a);
    printBinary(&b);

    getDecimalSet(&a);
    printf("A = ");
    printDecimalSet(a.set);

    getDecimalSet(&b);
    printf("B = ");
    printDecimalSet(b.set);

    getSetsIntersection(a.set, b.set, &result);
    printf("A ⋂ B = ");
    printDecimalSet(result.setsIntersection);

    getSetsUnion(a.set, b.set, &result);
    printf("A ⋃ B = ");
    printDecimalSet(result.setsUnion);

    getSetsDifference(a.set, b.set, &result);
    printf("A \\ B = ");
    printDecimalSet(result.setsDifference);

    getSetsSymmetricDifference(a.set, b.set, &result);
    printf("A △ B = ");
    printDecimalSet(result.setsSymmetricDifference);

    return 0;
}
