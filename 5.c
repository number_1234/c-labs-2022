#include <stdio.h>
// task5

void hello(char s[21]) {
    if (s[0] != '\n') {printf("Hello, %s\n", s);}
    else {printf("Entered invalid string (\\n), try again!\n");}
}


int main(void) {
    char s[21];

    printf("Enter string ( <= 20 chars): ");
    fgets(s, 21, stdin);
    hello(s);

    return 0;
}
