#include <math.h>

int loopFib(int n) {
    int f1 = 1, f2 = 1;
    long result;

    if (n == 0) {
        return 0;
    } else if ((n == 1) || (n == 2)) {
        return 1;
    } else {
        for (int i = 0; i < n-2; i++) {
            result = f1 + f2;
            f1 = f2;
            f2 = result;
        }
    }
    
    return result;
}


int recursionFib(int n) {
    int result;

    if (n == 0) {
        result = 0;
    } else if ((n == 1) || (n == 2)) {
        result = 1;
    } else {
        result = recursionFib(n-1) + recursionFib(n-2);
    }

    return result;
}


double binetFormulaFib(int n) {
    double partOne, partTwo;

    partOne = pow(((1 + sqrt(5)) / 2), n);
    partTwo = pow(((1 - sqrt(5)) / 2), n);
    return (partOne - partTwo) / sqrt(5);
}
