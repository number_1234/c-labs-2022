#include "fibFunctions.h"
#include <math.h>
#include <stdio.h>
// task 4, fib

void getInput(int *n) {
  printf("Enter n (int): ");
  scanf("%d", n);
}

int main(void) {
  int n;
  unsigned flag = 0;

  while (!flag) {
    getInput(&n);
    // при n > 46 рекурсия начинает врать; формула Бине - вроде даже ещё  раньше
    if ((n < 0) || (n > 46)) {printf("Invalid n entered,\nenter another value.\n\n");}
    else {flag = 1;}
  }

  printf("Fib_%d = %d (loop function)\n", n, loopFib(n));
  printf("Fib_%d = %.15g (Binet formula)\n", n, binetFormulaFib(n));
  printf("Fib_%d = %d (recursion)\n", n, recursionFib(n));

  return 0;
}