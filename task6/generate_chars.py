#! /bin/python3
import sys
from random import choice


chars = [choice("qwertyuiopasdfghjklzxcvbnm") for _ in range(int(sys.argv[1]))]
with open(sys.argv[2], "w", encoding="utf-8") as f:
    f.write("".join(chars))
