#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "mySort.h"
// sort-c, массив символов

const char HELP_MESSAGE[] = "Usage:\n./sort [-s|-b|-i|-q|-h] [-t|-2] <file>";
const char *OPTIONS[7][2] = {
    {"-s", "Use Selection-sort."}, // ok
    {"-b", "Use Bubble-sort."}, // ok
    {"-i", "Use Insertion-sort."}, // ok
    {"-q", "Use Quick-sort (default)."}, // ok
    {"-h, --help", "See help-message and exit."}, // ok
    {"-t | -2", "Read file as txt or as binary (-t by default)."}, // ok
    {"<file>", "Path to input-file."}, // ok
};


void printHelpMessage(void) {
    printf("%s\n", HELP_MESSAGE);
    for (int i = 0; i < 7; ++i) {printf("%s   %s\n", OPTIONS[i][0], OPTIONS[i][1]);}
}


struct Args {
    char sortFlag;
    char *filePath;
    char *fileMode;
};


bool _valueInArray(char arr[4][1], char value) {
    for (int i = 0; i < 4; ++i) {
        if (arr[i][0] == value) {return true;}
    }
    return false;
}


void parseArgs(struct Args *args, int argc, char **argv) {
    if (argc == 1) {
        printf("Not enough arguments, exiting.\n");
        exit(0);
    } else {
        if ((argv[1][0] == '-') && ((argv[1][1] == 'h') || !(strcmp(argv[1], "--help")) || !(strcmp(argv[1], "help")))) {
            printHelpMessage();
            exit(0);
        }

        char sortFlagValues[4][1] = {'s', 'b', 'i', 'q'};
        args->sortFlag = 'q';
        args->fileMode = "r";
        
        if (argc == 2) { // ok
            if (argv[1][0] != '-') {args->filePath = argv[1];} // есть только файл
            else {
                printf("No file provided, exiting.\n");
                exit(0);
            }
        } else if (argc == 3) {
            if (argv[2][0] == '-') { // нету файла 
                printf("No file provided, exiting.\n");
                exit(0);
            } else { // есть файл и один аргумент (смотрим, какой он)
                if (_valueInArray(sortFlagValues, argv[1][1])) {args->sortFlag = argv[1][1];}
                args->filePath = argv[2];
            }
        } else if (argc == 4) {
            if (_valueInArray(sortFlagValues, argv[1][1])) {args->sortFlag = argv[1][1];} // ok

            if (argv[3][0] != '-') {args->filePath = argv[3];}
            else {
                printf("Invalid argument: %s\nTry './sort --help' for more information.\n", argv[3]);
                exit(0);
            }
        }

        for (int i; i < argc; ++i) {
            if (argv[i][0] == '-') {
                if (argv[i][1] == 't') {args->fileMode = "r";}
                else if (argv[i][1] == '2') {args->fileMode = "rb";}
            }   
        }

        if (access(args->filePath, F_OK) != 0) {
            printf("File %s does not exist!\n", args->filePath);
            exit(0);
        }
    }
    
}

/*void parseArgs(struct Args *args, int argc, char **argv) { // deprecated
    if ((argc != 4) || (argc == 1)) {
        printf("Not enough arguments, exiting.\n");
        exit(0);
    } else if ((argv[1][1] == 'h') || !(strcmp(argv[1], "--help"))) {
        printHelpMessage();
        exit(0);
    } else {
        char sortFlagValues[4][1] = {'s', 'b', 'i', 'q'};

        if (argv[1][1] == 'h') {
            printHelpMessage();
            exit(0);
        } else if (!_valueInArray(sortFlagValues, argv[1][1])) {argv[1][1] = 'q';}

        switch (argv[2][1]) { // file format
            case '2':
                args->fileMode = "rb";
                break;
            case 't':
                args->fileMode = "r";
                break;
            default:
                argv[2][1] = 't';
                args->fileMode = "r";
        }

        if (access(argv[3], F_OK) != 0) {
            printf("File %s does not exist!\n", argv[3]);
            exit(0);
        }
    } 
}*/


void countChars(FILE *file, struct Array *arr) {
    arr->size = 0;
    while (getc(file) != EOF) {arr->size++;}
}


void createArray(struct Array *arr) {
    arr->values = (char *)malloc(arr->size * sizeof(char));
    if (arr->values == NULL) {
        printf("Memory was not allocated, exiting...\n");
        exit(0);
    }
}


void getInput(FILE *file, struct Array *arr) {
    for (int i = 0; i < arr->size; ++i) {fscanf(file, "%c", &arr->values[i]);}
}


int main(int argc, char **argv) {
    struct Args args;
    parseArgs(&args, argc, argv);

    struct Array arr;
    printf("%s\n", args.fileMode);
    FILE *inputFile = fopen(args.filePath, args.fileMode); // ok

    countChars(inputFile, &arr);
    // printf("PID = %d\n", getpid());
    createArray(&arr);
    rewind(inputFile);
    getInput(inputFile, &arr);
    fclose(inputFile);

    switch (args.sortFlag) { // ok
        case 's':
            printf("Selection-sort\n");
            selectionSort(&arr);
            break;
        case 'b':
            printf("Bubble-sort:\n");
            bubbleSort(&arr);
            break;
        case 'i':
            printf("Insertion-sort:\n");
            insertionSort(&arr);
            break;
        default:
            printf("Quick-sort:\n");
            quickSort(&arr, 0, arr.size-1);
    }

    for (int i = 0; i < arr.size; ++i) {printf("%c", arr.values[i]);}

    return 0;
}
