#include <stdio.h>

struct Array {
    size_t size;
    char *values;
};


void _swap(char *a, char *b) {
    char tmp = *a;
    *a = *b;
    *b = tmp;
}


void selectionSort(struct Array *arr) {
    for (int i = 0; i < arr->size - 1; ++i) {
        for (int j = i + 1; j < arr->size; ++j) {
            if ((int)arr->values[i] > (int)arr->values[j]) {
                _swap(&arr->values[i], &arr->values[j]);
            }
        }
    }
}


void bubbleSort(struct Array *arr) {
    for (int i = 0; i < arr->size; ++i) {
        for (int j = 0; j < arr->size-1; ++j) {
            if ((int)arr->values[j+1] < (int)arr->values[j]) {
                _swap(&arr->values[j], &arr->values[j+1]);
            }
        }
    }
}


void insertionSort(struct Array *arr) {
    for (int i = 0; i < arr->size; ++i) {
        int j = i - 1;
        while ((j >= 0) && ((int)arr->values[j] > (int)arr->values[j+1])) {
            _swap(&arr->values[j], &arr->values[j+1]);
            j--;
        }
    }
}


int _partition(struct Array *arr, int left, int right) {
    char v = arr->values[(left + right) / 2];
    int i = left;
    int j = right;

    while (i <= j) {
        while ((int)arr->values[i] < (int)v) {i++;}
        while ((int)arr->values[j] > (int)v) {j--;}
        if (i >= j) {break;}
        _swap(&arr->values[i++], &arr->values[j--]);
    }
    return j;
}


void quickSort(struct Array *arr, int left, int right) {
    if (left < right) {
        int q = _partition(arr, left, right);
        quickSort(arr, left, q);
        quickSort(arr, q+1, right);   
    }
}
