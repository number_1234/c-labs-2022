#include <stddef.h>
#include <stdlib.h>

struct Array {
    size_t size;
    char *values;
};

void selectionSort(struct Array *arr);
void bubbleSort(struct Array *arr);
void insertionSort(struct Array *arr);
void quickSort(struct Array *arr, int left, int right);